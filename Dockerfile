FROM node:18.13-slim

# Defina o diretório de trabalho
WORKDIR /app

# Copie o arquivo package.json para o diretório de trabalho
COPY package.json ./

# Instale as dependências do aplicativo
RUN yarn install --force

# Copie o resto dos arquivos para o diretório de trabalho
COPY . .

CMD ["yarn", "dev"]

# Defina a porta padrão
EXPOSE 3333

