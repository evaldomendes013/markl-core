import {test, expect} from "@playwright/test";

test('Cadastrar nova tarefa', async ({page, request}) => {
    const  taskName = 'Criando Tarefa com Playwright'

    await request.delete('http://localhost:3333/helper/tasks/' + taskName)

    await page.goto('/')
    await page.fill('input[class*=InputNewTask]', taskName)
    await page.click('button[class*=ButtonNewTask]')

    const locator = page.locator(`css=.task-item p >> text=${taskName}`)
    await expect(locator).toHaveText(taskName)
})